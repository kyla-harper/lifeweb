import React from 'react'
import './Product.css'

function Product (props) {
  return (
    <div className="product">
      <input value={props.cost} />
      <img className='image' src={props.image} alt={props.name} />
      <h1>{props.quantity}</h1>
      <button onClick={() => props.onClick(props)} disabled={props.disabled}>Buy</button>
    </div>
  )
}

export default Product
