import React from 'react'
import './VendingMachine.css'
import Product from './Product'

class VendingMachine extends React.Component {
  handleMoneyInput (event) {
    this.setState({
      insertedAmount: event.target.value
    })
  }

  handlePurchase (product) {
    const difference = this.state.insertedAmount - product.cost
    const newRemainder = this.state.remainderAmount + difference

    const products = this.state.products.slice()
    const index = products.findIndex(item => item.name === product.name)
    products[index].quantity--

    this.setState({
      insertedAmount: 0.00,
      remainderAmount: Math.round(newRemainder * 100) / 100,
      products: products
    })
  }

  refundCash() {
    this.setState({
      insertedAmount: 0.00,
      remainderAmount: 0.00
    })
  }

  constructor (props) {
    super (props)
    this.state = {
      insertedAmount: 0.00,
      remainderAmount: 0.00,
      products: props.products
    }

    this.handleMoneyInput = this.handleMoneyInput.bind(this)
    this.handlePurchase = this.handlePurchase.bind(this)
  }

  render () {
    const productsList = []
    const inserted = this.state.insertedAmount
    const remainder = this.state.remainderAmount

    this.state.products.forEach(product => {
      productsList.push(
        <Product
          key={product.name}
          name={product.name}
          image={product.image}
          quantity={product.quantity}
          cost={product.cost}
          disabled={inserted + remainder < product.cost || product.quantity === 0}
          onClick={(product) => this.handlePurchase(product)}
        />
      )
    })
    
    return (
      <div className="vending-machine">
        <section className="section products">
          {productsList}
        </section>
        <section className="section money-controls">
          <input value={this.state.insertedAmount} onChange={this.handleMoneyInput} />
          <p>${this.state.remainderAmount}</p>
          <button onClick={() => this.refundCash()}>Refund</button>
        </section>
      </div>
    )
  }
}

export default VendingMachine
