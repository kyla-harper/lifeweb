import React from 'react'
import ReactDOM from 'react-dom'
import VendingMachine from './VendingMachine'

import mrGoodbar from './images/mr-goodbar.jpg'
import peanutButterCups from './images/peanut-butter-cups.jpg'
import snickers from './images/snickers.jpg'
import twix from './images/twix.jpg'

const products = [
  {
    name: "Snickers",
    image: snickers,
    quantity: 15,
    cost: 1.60
  },
  {
    name: "Reeses's' Peanut Butter Cups",
    image: peanutButterCups,
    quantity: 20,
    cost: 2.00
  },
  {
    name: "Mr. Goodbar",
    image: mrGoodbar,
    quantity: 300,
    cost: 1.00
  },
  {
    name: "Twix",
    image: twix,
    quantity: 8,
    cost: 1.80
  }
]

ReactDOM.render(
  <VendingMachine products={products} />,
  document.getElementById('root')
)
